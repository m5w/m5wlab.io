title: NASA/SpaceX DEMO-2, protests and a pandemic
date: 2020-05-30 21:00
updated: 2022-01-09 16:00
category: Thoughts
tags: NASA, SpaceX, space flight, civics, blm
slug: nasa-spacex-protests-pandemic
Status: published

For the first time in nearly a decade, a manned rocket took off from the Western Hemisphere and entered outer space. The second demonstration flight for the NASA/SpaceX manned rocket (DEMO-2) successfully launched and is currently orbiting our little bubble of life. 

I was one of those kids that dreamed of being an astronaut. Space flight is such an exacting, unforgiving endeavour and a marvel to see celebrated in Cape Canavral once again. I was a 10 year old again, heart racing as the seconds ticked down, holding my breath as the rocket made its way through maximum pressure and into the Exosphere. 

I think what makes this launch even more impressive is that, in the end, it felt so ordinary. The whole process is automated today, the complicated mathematics and adjustments and *flying* all handeled by software and sensors and servos. Watching the live feed, it's 10 minutes into the flight and the SpaceX control room is mostly handshakes and hugs right now. 20 years ago operators would have been laser-focused on their terminals (and I'm sure they are still at NASA), but today computers are mostly running the show, even the executive parts. It's really something to see. 

But as Crew Dragon circles the globe, cities across the US are on fire as people express their pain and try to take their voices back from a system that denies them equality. This against the backdrop of skyrocketting unemployment, death and suffering from a global coronavirus pandemic. 
Countries around the world have been grappling with a rise in nationalism and introversion. Populations polarize, inequality grows. In the US, both the inconsistently managed coronavirus pandemic response, and recent police-caused death (murder) of George Floyd in Minneapolis, are illuminating the overdue need for correcting our lack of real, honest equality.

----
A though in 2022

The international space cooperation has crossed borders and united people in ways few other diplomatic programs have. During the height and waning years of the Cold War between the Soviet Republic and United States, the space programs still regularly worked together on joint scientific programs, sharing their knowledge to improve each other's platforms. 

But it's also incredibly exclusive. Only a few countries able to afford their own domestic launch programs, and just a dozen or so have fully fledged independent space programs. Hardly universal.

Space programs can also easily suffer the same problems as all other social goods. [Critical review of the historical inequities of the Space Age](https://www.history.nasa.gov/sp4801-chapter22.pdf) are not hard to find. And to the observation that sparked this article, it's tantalizing for these agencies to commercialize and sell their social good off to industry and capitalize the future of a society.

And those commercial firms are market-driven, and beholden to maximize the performance of their shareholders' capital. So they'll [launch satelites that hamper terrestrial astronomy](https://iopscience.iop.org/article/10.3847/2041-8213/ac470a), or [unironically build a program around flinging One Percenters into space with a giant sex toy](https://www.newswars.com/bezos-flies-into-space-on-giant-dildo/).

Like the early web, the best Outer Space is an unbiased, shared, accessible, carefully cared for one. Like the web, Space can also be commercialized and dominated by private entities allowed to run faster than few exclusive regulators able to afford to have programs. 

Maybe there's a lesson here for things that we let loose to private profit motivations, or for exclusive early science that we fail to be good shepherds of. 