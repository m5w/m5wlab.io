title: Gerrymandering, the great radicalizer
date: 2022-01-09 17:00 
updated: 2022-01-09 17:00  
category: Thoughts
tags: politics 
slug: gerrymandering
Status: draft

Thesis: Gerrymandering encourage radicalization of a political body. The US becomes less democratic and more radically unstable the longer gerrymandering is allowed to occur. 

