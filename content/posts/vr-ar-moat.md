title: VR, AR and jumping the moat
date: 2020-11-20 21:00
updated: 2020-11-20 21:00
category: Thoughts
tags: VR, AR, tools, 
slug: vr-ar-jumping-moat
Status: draft

Thesis: Successfully crossing the evolutional moat requires deep field knowledge of the existing side and speculative attempts to build a solution natively for the other side, instead of improving a system for the known side until it reaches across. 


Tech evolutions tend to come with moats. 

Self-driving car systems are rated on a scale of 1-6. A Level 1 system describes a cruise control system with some lane keeping features, while Level 6 is a system that fully operates the car and makes decisions during unique events. The endpoint is, interestingly, an entirely new form of locomotion. 

Telephones have had a similar evolution. The rotary phone begot incremental improvements as touchtone and then voice over IP technologies became mainstream. The original cell phone was, to be too simple, a handset with a transmitter and battery. Fast-forward 20 years and the pocket (or wrist) computers we now carry resemble their ancestors only in quaint skeuomorphic ways. 

Each of these had a moat. 

- Moving knowledge from one person to the next was a purely oral thing, and still mostly done in alegory and story, until written language (or the mass production of printed text) was invented. 
- Getting from telephone + features to mobile personal connected computer didn't happen until Apple launched its keyboardless mobile device. (sorry for the iPhone reference, I hate me too).
- Getting from Level 3 (pretty much drives itself but the driver needs to pay attention) to Level 4 (no driver attention needed in a restricted geographic space) has turned out to be exceptionally difficult. 
 

After each of these a fundamental shift happens in the social consciousness, the human-tool symbiotic 
