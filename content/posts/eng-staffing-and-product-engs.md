title: Let job titles be aspirational and start hiring product engineers
date: 2025-02-17
updated: 2025-02-27
category: Thoughts
tags: engineering, strategy, staffing, leadership, hot-takes, ai, product-engineer
slug: full-stack-and-product-engineers
status: published
thesis: It's not one or the other; full stack engineers are a necessary and versatile part of a healthy enginering org's composition. And that composition is about to get an additional category. 

This week's Hot Take on LinkedIn is a theme on retiring the "Full Stack" role in software engineering. Some folks looking for engagement are talking about how "full stack" engineers are either not experienced enough across an org's stack, are actually just front- or back-end devs with a different name, or are somehow "lazy" engineers that collect the leftovers (this last one is new for me). 

Let me share a different perspective on this: the "full stack" label isn't the problem, but rather that it's how these folks are thinking about software engineering roles in general.

Why isn't "full stack" the problem? Because none of the role categories are a problem. The problem is thinking at the wrong altitude levels where where and how you're hiring.

Who can relate to the struggle balancing "enough" front-end work or back-end work for a team because product doesn't have equally juicy problems for both the wizards and geniuses in a given moment? That team's product manager ends up pulling lower-impact work into the stream in order to keep team member utilization high, and you start seeing "work" getting done instead of high-impact work.

A full-stack engineer isn't meant to be an expert in everything, but someone that effectively works across the stack in their team's responsibilities to solve problems.

When you're considering staffing changes, look at a few levels: team, cross-team, and cross-org.

When I'm expanding a team, I first look to hire people who are full-stack-focused, unless that team is particularly specialized in one area. I'm not looking for unicorns who are experts in everything from kernel optimization to pixel-perfect CSS, but engineers who can:

1. Understand how different parts of the system interact
2. Make informed architectural decisions that consider both front and back-end implications
3. Collaborate effectively with specialists when deeper expertise is needed
4. Take ownership of features from concept to deployment

If I'm looking at creating new capability across teams or across the organization, specialists become much more interesting—and will probably be much more interested in the role too! A Rust wizard dropped into a feature team that has them doing filler work 40% of the time is going to be looking for a new role quickly.

But something is indeed changing. _Key dramatic music._

As we move toward AI-augmented development, I believe we'll see the accelerating emergence of "product engineers" - full-stack developers who combine technical versatility with strong user/product motivation. These engineers won't just work across the stack - they'll bridge the gap between technical implementation and user needs, leveraging AI tools to augment their capabilities across the spectrum.

I referenced in my [recent note on Junior engineers](/junior-engineers-ai-and-the-economic-cycle.html), that while AI isn't going to replace engineers, what it already has started doing is refactoring some work tasks into a set of "function calls" available to engineers at all levels. Writing unit tests, boiler-plating and common operations, bug fixing, and many other run-of-the-mill tasks go pretty quick with an LLM. It can close basic knowledge gaps with new technologies. It can even help with architectural direction. Heck, Claude did most of the code generation for a few hobby projects for me recently. But key to using these tools is knowing when they're wrong, and knowing what you want in the first place. That's where the "product engineer" comes in. We'll talk about that in another note though. 

So, the key isn't to "respect specialization again," it's to affirm that engineering organizations need different compositions of skills. Sometimes you need deep specialists, sometimes you need versatile generalists, and often you need both working together, at different times.

Instead of retiring "full stack," let's evolve how we think about engineering roles to reflect the actual work being done - and how that work is changing with new technologies and approaches.

How do you approach expertise strategy for your orgs? What's your take on how AI might reshape these traditional engineering role definitions? Join or start the conversation over on [LinkedIn](https://www.linkedin.com/posts/mattlutze_saas-softwaredevelopment-cto-activity-7297232440220143616-1jNA) or [Bsky](https://bsky.app/profile/lutze.bsky.social/post/3lifg2docps2j) <3