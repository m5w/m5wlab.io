title: Graduating and privledge and markets
date: 2020-06-01
updated: 2022-02-19
category: Thoughts
tags: university, social responsibility, 
slug: graduation
Status: published

I graduated from Harvard Extension School on Thursday. The school and Harvard University did their best with exceptional times and had a nice virtual ceremony with lots of joy and cheer. Conan O'Brien did a bit, I think. My emotions were a little mixed, which felt selfish for all the real problems pepople are facing.

The ALM in Software Engineering filled 6 years of my nights and weekends, replacing my social life for months at a time. When it got too frustrating I'd look forward to a few milestones, like commencement or acceptance of my thesis, as asiprational goals to validate the struggle. Sitting in my house watching deans and public figures try to bring the hype over YouTube, complete with B Roll of former graduation classes processing or cheering, didn't feel like the finish line I'd expected. 

It looks like I had two final lessons to learn this week. 

First, that ego can useful in very small doses and otherwise generally counterproductive. There's a lot of wonderful people and opportunities in my life. Ceremonies and self-recognition are nice and also such silly privileged events. I feel a bit embarrassed about how fortunate I am to have so much and hope I can find ways to share that fortune with the world. 

Second, on market timing. I finished my Bachelor degree just as the Great Recession hit, and now I finish my Master degree as a coronavirus pandemic decimates and global markets receed. At the risk of making light of what has been an awful situation, if I do some day forget these warnings and try again, buy gold and run for the hills.