title: Being better
date: 2020-01-16 21:00 
updated: 2020-01-16 21:00 
category: Thoughts
tags: blm, equity, equality, humanity 
slug: being-better
Status: draft

Thesis: The grace of consciousness is the opportunity to be better than we were yesterday. As every voice gains access to the collective species consciousness, we must do a better job defining "better."

