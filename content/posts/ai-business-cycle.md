title: Junior engineers, AI, and the business cycle
date: 2025-02-14
updated: 2025-02-14
category: Thoughts
tags: engineering, strategy, AI, startups, growth, leadership, culture
slug: junior-engineers-ai-and-the-economic-cycle
status: published
thesis: The current narrative about AI reshaping software engineering masks a typical economic cycle, where the real challenge isn't AI replacing engineers but maintaining healthy team structures during a market downturn.

The narrative around AI's impact on software engineering needs a reality check and is missing a key current condition.

Having managed software teams through multiple tech transitions, I've noticed a pattern: we tend to oversimplify complex industry shifts into clean narratives that rarely match reality.

What's that missing condition? It's the economy, silly.

When growth tightens, hiring slows and staff reductions pick up. They'll talk about AI revolutionizing development and allowing them to make teams more efficient, but as often as not they're using that tech as a smoke screen to reduce shareholder anxiety. Usually they over-correct, creating a dangerous expertise vacuum when their senior staff then churn—and then hiring starts again when markets relax and growth returns.

The value ceiling for individual contributors hasn't changed. What has changed is the definition of valuable engineering work. Engineers who can identify hidden needs, predict future requirements, and successfully advocate for their inclusion in product timelines remain absolutely critical.

AI isn't replacing engineering—it's turning some work into a function call we can reference. The real engineering challenges remain distinctly human: taking on poorly defined problems and delivering efficient solutions within the business's problem space.

About those laid-off engineering managers: strong technical leadership has always been valuable precisely because it's rare. Companies letting go of experienced managers are likely creating future technical debt by sacrificing the very people who understand how to bridge business needs with technical execution.

So to all the new devs out there: AI isn't eating your future. This has as much to do with the macro business cycle as it does to AI tools adjusting what you'll do in your day-to-day.

And to my peers: Junior Engineers aren't just "cheap labor to do the boring stuff" but should be the lifeblood of your engineering organization.

When I can, I primarily hire juniors. I look for deeply curious, humble, driven people, and bring them into teams where they learn through empowerment, ownership and collaboration. They usually end up driving the teams as much as their more senior mentors.

The market isn't facing an AI apocalypse. It's experiencing a natural evolution that demands we think more critically about how we build and maintain engineering teams.

If you're wrestling with these challenges in your organization, let's connect. After nearly 20 years in the industry, I've learned that the best solutions often come from challenging our assumptions about what "has to be" true.