title: 2%
date: 2024-11-12
category: Thoughts
tags: humanity, society, choice, activism
slug: be-the-2-percent
status: draft
thesis: Each of us has the opportunity to make a difference. Pick one thing you feel most passionate about improving, and with just 2% of your time you'll make a measurable difference.

A lot of people are frustrated with the world today. Populism and nationalism are on the rise; individualism and isolationism are celebrated as virtues; the climate and inequality are ignored or dismissed. Sounds pretty dire. 

We want to see a change, but we don't know how to follow [Arleen Lorrance's](https://quoteinvestigator.com/2017/10/23/be-change/) advice. Maybe it feels too big, maybe it feels like the momentum is against us. It can feel like inaction is the only rational choice. 

Here's my challenge to you: *Give 2% of your time to one thing for 6 months.*

Let's do a little math. There are 168 hours in a week. On average week, we 

* sleep, rest, & hygiene 9*7 = 63 hours,
* work & commute 9*5 = 45 hours,
* eat 2*7 = 14 hours.

We end up with 168 - 63 - 45 - 14 = 46 hours for other activities. 

So where does this 2% come from? Two percent of the 168 hours in the week is just 3.36 (let's say 3.5) hours. Even just looking at the 46 hours we have for other activities, 3.5 hours is just 8% of your free time. It doesn't feel like much when we look at it like that--and this can make it feel very accessible. 

Before we get into how to use this 2% of your time, let's think about where you'll spend it. And to do that, you're going to pick your One Thing. 

Step 1: Look at the world around you 

Set aside 30 minutes. Get your pen and paper, open a fresh Note, or fire up your typewriter, and make a list for the following statements. It's ok for items to be on multiple lists. It's also ok to break this up into multiple sessions; work on it for 15 minutes, take a break, and then come back to it. 

1. __ isn't right in the world.
1. __ could be improved.
1. __ need help.
1. __ would be better if __.

Step 2: Look inside

Set that first list aside, and let's think about what you bring to the table. For these questions, think about experiences you've had a school, at work, in your family, with friends, in your community, etc. Open up your mind to Give yourself up to another 30 minutes. 

1. I'm good at: 
1. I enjoy doing: 
1. I'm passionate about: 
1. I feel pressure or stress when I think about: 

Step 3: 