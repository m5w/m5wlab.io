title: Impatient Optimism 
date: 2022-02-20 
updated: 2022-02-20  
category: Thoughts 
tags: optimism, disruption
slug: impatient-optimism 
Status: draft 

Thesis: The optimist drives progress. The impatient optimist enables disruptive change. 

Of all the -ists that a person might be, I think the the Optimist is 

[Health benefits](https://www.health.harvard.edu/mind-and-mood/thoughts-on-optimism)




[Far from naïve](https://www.warpnews.org/essays/are-optimists-naive-no-but-the-pessimists-are/), the optimist's compulsion to see the world as it could or should be drives the producitive amongst their ranks to be the change needed to achieve that vision. 



https://twitter.com/maxcroser/status/1348971608799645697?s=21


