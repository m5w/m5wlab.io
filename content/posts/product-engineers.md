title: Product engineers
date: 2020-02-
updated: 2020-02- 
category: Thoughts
tags: blm, equity, equality, humanity 
slug: being-better
Status: draft


Product engineers will be our full-stack + AI savy user experts. The product engineer is differentiated by a drive to build great products as an end goal, instead of great software as an end goal. Both folks are important, but where the full-stack or back-end engineer is driven to deliver the most optimal software for constraints, the product engineer is looking to deliver the most optimal feature. Think of it like an entrepreneurial engineer, or a product manager that spent part of their career slinging code and gets back into it. AI tools will likely give rise to this kind of hybrid engineer, especially for startups, as the tools will help fill skill gaps that their knowledge or perspective would otherwise allow them to develop with. 
