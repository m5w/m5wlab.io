title: Improving the impact of an argument
date: 2021-01-10 
updated: 2021-01-10
category: Thoughts
tags: psychology, persuasion, improvement 
slug: improving-impact-argument
Status: draft


Thesis: To improve a persuasive position, fewer strong arguments is better than a comprehensive list of strong and weaker arguments. 


Niro Sivanathan's TED talk - research shows that diluting strong arguments with additional weak ones reduces the value of the overall position. Example includes people valuing more a smaller dishware set than a larger one with a few damaged pieces 

In persuasion, the effect with your audience is what matters, and you're not necessarily the audience. 

Reference stand-up and honing their sets


A strategy: 

Get your list of arguments together; 5 Ps
Describe the person or people, your audience, that you hope to persuade. Audience analysis is critical for rhetorical accuracy.
Pre-screen your list for any arguments that may be interesting to you but are unlikely to be important to your audience. We want a tight 5, not a meandering 8. 
Test your pitch with someone or multiple people near in profile to the audience but not a part of it, to see if you can improve more. Confirm or invalidate our assumptions like good scientists. 
In the moment, remind yourself to not feel like you have to continually justify your position; this weakens your overall standing. 


