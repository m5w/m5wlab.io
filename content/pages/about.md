title: About
date: 2020-05-30
updated: 2024-11-12


Hi, I'm Matthew.

I work with a great group of folks at Bitly, leading engineering teams that build the infrastructure for the world's most popular link management platform.

Please feel welcome to [send me an email](mailto:collab@lutze.co) or say hi on my social network links. 

I also have a small presence around the web:

* [LinkedIn](https://www.linkedin.com/in/mattlutze)
* [Github](https://github.com/lutze)
* [Quora](https://www.quora.com/profile/Matt-Lutze)
* [Medium](https://medium.com/@lutze)
* [Some small side projects](http://lutze.co/projects)


## Colophon

This site is a set of [Markdown](https://daringfireball.net/projects/markdown/syntax "Markdown syntax website") and [reStructuredText](https://docutils.sourceforge.io/rst.html "reStructuredText syntax website") documents compiled using [Python](https://www.python.org/ "Python organization homepage")-based [Pelican static site generator](https://blog.getpelican.com/ "Pelican homepage").

The theme for this site evolved from Mathieu Agopian's[Thème mnmlist](http://mathieu.agopian.info/mnmlist/theme.html "theme port website") port of Leo Babauta's [mnmlist](https://mnmlist.com/ "Link to the blog mnmlist") blog.