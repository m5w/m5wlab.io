#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Matthew'
SITENAME = 'm5w'
SITEURL = ''
BASEURL = 'https://m5w.gitlab.io'

THEME = 'themes/mnmlist'

PATH = 'content'
OUTPUT_PATH = 'public'
ARTICLE_PATHS = ['posts']
USE_FOLDER_AS_CATEGORY = True
DEFAULT_CATEGORY = 'Thoughts'

TIMEZONE = 'Europe/Berlin'
DEFAULT_LANG = 'en'

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (('About', '/pages/about.html'),)

# Social widget
SOCIAL = (('Twitter', 'https://twitter.com/matislearning'),
          ('LinkedIn', 'https://linkedin.com/in/mattlutze'),)

DEFAULT_PAGINATION = 5

# Uncomment following line if you want document-relative URLs when developing
#RELATIVE_URLS = True
